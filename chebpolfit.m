function [c,h,x] = chebpolfit(f, N, a, b)
%%CHEPOLFIT Fit a function f (given as a handle) using N-th order Chebyshev polynomials. 
%%The fitting concerns the inverval [a b].
% 
% c are the N+1 coefficients of the chebyshev polynomials - one for each polynomial
% h are the N+1 coefficients of the fitted polynomial (computed by adding
% for each power the respective chebyshev coefficients)
% x are the N+1 chebyshev nodes 
% 
% http://math.tut.fi/~piche/numa2/lecture17.pdf

N = N + 1; 

if ~exist('a', 'var'), a = -1; end
if ~exist('b', 'var'), b = 1; end
    
% % scale function from [a b] to [-1 1]
g = @(x) f( (b-a)*x/2 + (a+b)/2);

% the Chebyshev nodes (points where the second derivative is zero)
x = cos((2*(1:N)'-1)*pi/2/N);
y = feval(g, x);

% the regression coefficients
c = zeros(1,N); 

% the Chebyshev polynomials of type 1
T = zeros(N,N);

for j = 1:N
    
    if j == 1,
        T(:,1) = ones(N,1);
        c(1) = sum(y)/N;
        
    elseif j == 2,
        T(:,2) = x;
        c(j) = sum( T(:,2) .* y) * 2 / N;
        
    else
        T(:,j) = 2*x.*T(:,j-1) - T(:,j-2);        
        c(j) = sum( T(:,j) .* y) * 2 / N;
    end
end

% the coefficients of the resulting polynomial
h = zeros(1,N);
for j = 0:N-1,
    h(1:j+1) = h(1:j+1) + wrev(chebpoly(j))*c(j+1);
end

% rescale polynomial by a factor of 2/(b-a) in the x-direction
tmp = polyrescl(wrev(h), (b-a)/2, 1);

tmp = polyreloc(tmp, (a+b)/2);
h = wrev(tmp);

end
