% This scirpt is used by Mario to test the Edge Variant FIR filter for the
% distributable implementation

clear all;
close all;

%% Graph Generation

G = gsp_community();
paramplot.show_edges = 1;
gsp_plot_graph(G,paramplot);

N = G. N; % number of nodes
G = gsp_compute_fourier_basis(G);

% Laplacian
Ld = full(G.L);

% degree matrix
D= diag(diag(Ld));

% normalized Laplacian
L_norm = D^(-1/2)*Ld*D^(-1/2);

% eigendecomposition
[U, Lam] = eig(L_norm);

% The graph signal is all 1 in the Graph Fourier domain
xf = ones(N,1);
% graph signal in the vertex domain
x = U*xf;

%% Normal Filter parameters
close all,
% the frequency response will be a step function with cut-off 1 in a [0,2]
% uniform spectrum corresponding to the normalized Laplacian

% cut-off
lc = 1;

% desired step - response
dresp = @(lambda)double(lambda<lc);

% desired heat kernel - response
gamma = 3; % weight parameter
% drespKer = @(lambda)(1./(1+gamma*lambda));
drespKer = @(lambda)(exp(-gamma*(lambda - 0.75).^2));

% FIR design via LS(i.e., polyfit)

% Full order
Kfull = 25;

% universal grid
lam = linspace(0,2);

% step-poly
pol = polyfit(lam, dresp(lam), Kfull);
pol = wrev(pol); % put in increasing order

% kernel-step
polKer = polyfit(lam, drespKer(lam), Kfull);
polKer = wrev(polKer); % put in increasing order

% plot the filter frequency response (Step Response)
figure;
stem(lam,dresp(lam))
hold on    
stem(diag(Lam),dresp(diag(Lam)),'x')
stem(lam,polyval(wrev(pol),lam))
stem(diag(Lam),polyval(wrev(pol),diag(Lam)),'x')
legend('Desired-universal','Desired-graph','Designed-universal','Designed-graph')
xlabel('\lambda_i')
ylabel('Frequency Response')

% plot the filter frequency response (Heat Kernel)
figure;
stem(lam,drespKer(lam))
hold on
stem(diag(Lam),drespKer(diag(Lam)),'x')
stem(lam,polyval(wrev(polKer),lam))
stem(diag(Lam),polyval(wrev(polKer),diag(Lam)),'x')
legend('Desired-universal','Desired-graph','Designed-universal','Designed-graph')
xlabel('\lambda_i')
ylabel('Frequency Response')

%%
close all,
% build the FIR response matrix
% FIR response matrix for both responses
stepResp = dresp(diag(Lam));
kernelResp = drespKer(diag(Lam));

L_k = cell(Kfull,1);
Hk = zeros(N,N);

L_k_Ker = cell(Kfull,1);
Hk_Ker = zeros(N,N);

for iK = 0 : Kfull
    tmp = pol(iK+1)*L_norm^(iK);
    Hk = Hk + tmp;
    L_k{iK + 1} = vec(tmp);  
    
    tmp = polKer(iK+1)*L_norm^(iK);
    Hk_Ker = Hk_Ker + tmp;
    L_k_Ker{iK + 1} = vec(tmp);  
end

Hk_Ker = U*diag(kernelResp)*U';
h_tilde = vec(Hk);
h_tilde_Ker = vec(Hk_Ker);

% filter the signal with the full filter step
yfull = Hk*x;
yfull_f = U'*yfull;

% filter the signal with the full filter heat kernel
yfull_Ker = Hk_Ker*x;
yfull_f_Ker = U'*yfull_Ker;

NMSEfull = (norm(yfull_f-stepResp).^2)./(norm(stepResp).^2);
NMSEfull_Ker = (norm(yfull_f_Ker-kernelResp).^2)./(norm(kernelResp).^2);

% step filter
figure;
stem(diag(Lam),polyval(wrev(pol),diag(Lam)))
hold on
stem(diag(Lam),yfull_f,'x')
legend('Designed-graph-polyval','Filtered Signal')

% heat kernel
figure;
stem(diag(Lam),polyval(wrev(polKer),diag(Lam)))
hold on
stem(diag(Lam),yfull_f_Ker,'x')
legend('Designed-graph-polyval','Filtered Signal')

%% Computation of all reduced order filters ( Kfilt <= Kfull )
close all,
stepPol = cell(Kfull,1);
kernelPol = cell(Kfull,1);

for kk = 1:(Kfull)
    
    % step-poly
    stepPol{kk} = polyfit(lam, dresp(lam), kk);
    stepPol{kk} = wrev(stepPol{kk}); % put in increasing order

    % kernel-step
    kernelPol{kk} = polyfit(lam, drespKer(lam), kk);
    kernelPol{kk} = wrev(kernelPol{kk}); % put in increasing order
    
end

stepHk = cell(Kfull,1);
kernelHk = cell(Kfull,1);

stepFull_y_f = nan(N, Kfull);
KernelFull_y_f = nan(N,Kfull);

stepNMSEfull = nan(Kfull,1);
kernelNMSEfull = nan(Kfull,1);

for pp = 0:(Kfull)
    for qq = pp:(Kfull)
        if qq ~= 0
            Ltmp = L_norm^(pp);
        
            tmpPoly = stepPol{qq};
        
            tmp = tmpPoly(pp+1)*Ltmp;
            if pp == 0
                stepHk{qq} = tmp;
            else
                stepHk{qq} = stepHk{qq} + tmp;
            end
    
            tmpPoly = kernelPol{qq};
            tmp = tmpPoly(pp+1)*Ltmp;
            if pp == 0
                kernelHk{qq} = tmp;
            else
                kernelHk{qq} = kernelHk{qq} + tmp;
            end
        end
    end
    
    if pp > 0
        
        stepFull_y = stepHk{pp}*x;
        stepFull_y_f(:,pp) = U'*stepFull_y;
    
        KernelFull_y = kernelHk{pp}*x;
        KernelFull_y_f(:,pp) = U'*KernelFull_y;
        
        stepNMSEfull(pp) = (norm(stepFull_y_f(:,pp)-stepResp).^2)./(norm(stepResp).^2);
        kernelNMSEfull(pp) = (norm(KernelFull_y_f(:,pp)-kernelResp).^2)./(norm(kernelResp).^2);
    end
end

figure,
p1 = plot(1:Kfull,stepNMSEfull,'-sr');
hold on, 
p2 = plot(1:Kfull,kernelNMSEfull,'-og');
legend([p1;p2],{'FIR (k) LP Filter','FIR (k) Heat Kernel'})
xlabel('Filter Order [k]')
ylabel('NMSE')
set(gca,'yscale','log'), grid on

%% Perform the filtering with reduced order
close all,
yred_f_step = nan(N,Kfull);
yred_f_kernel = nan(N,Kfull);
NMSEred_kernel = nan(Kfull,1);
NMSEred_step = nan(Kfull,1);
yred_kernel = zeros(N,1);
yred_step = zeros(N,1);

yred_f_step_nv = nan(N,Kfull);
yred_f_kernel_nv = nan(N,Kfull);
NMSEred_kernel_nv = nan(Kfull,1);
NMSEred_step_nv = nan(Kfull,1);

nnzL = nnz(L_norm);

Ls = sparse(L_norm);
Is = sparse(eye(N));

maskL = vec(Ls ~= 0);
maskEye = vec(eye(N)> 0);

tmp = kron(Is,Is);
As = tmp(:,maskEye);
As_nv = tmp(:,maskEye);

fprintf('\n\nGenerating Measurement Matrix...\n\n');
for kk = 1:Kfull
    tmp = kron(Ls^(kk-1),Is)*diag(vec(Ls));
    tmp = tmp(:,maskL);
    As = [As tmp];
    
    tmp = kron(Ls^(kk),Is);
    tmp = tmp(:,maskEye);
    As_nv = [As_nv tmp];
    
end

for Kred = 1:Kfull
    clear Phi
    clear phi_k
    % solving for kernel
    fprintf('\n\nSolving for EV-Filter Order %d...(Heat Kernel)\n\n',Kred);
    
    theta_star = As(:,1:N+Kred*nnz(Ls))\h_tilde_Ker;

    Phi0 = diag(theta_star(1:N));

    Phi = zeros(N,N,Kred);

    for kk = 1:Kred
        tmp = zeros(N,N);
        indx = N+1 + (kk-1)*nnz(Ls);
        tmp(maskL) = theta_star(indx : indx + nnz(Ls)-1);
        Phi(:,:,kk) = tmp;
    end
    
%     if isfinite(cvx_optval)
        yred_kernel = Phi0*x;

        for iK = 1 : Kred
            yred_kernel = yred_kernel + ...
                (Phi(:,:,iK) .* Ls)*Ls^(iK-1)*x;
        end
%     end

    % node variant
    Phi_nv = zeros(N,N,Kred+1);
    theta_star_nv = As_nv(:,1:(Kred+1)*N)\h_tilde_Ker;
    
    Hnv = zeros(N,N);
    for kk = 0:Kred
        indx = kk*N +1;
        Phi_nv(:,:,kk+1) = diag(theta_star_nv(indx: indx + N - 1));
        Hnv = Hnv + Phi_nv(:,:,kk+1)*Ls^kk;
    end
    
    yred_kernel_nv = Hnv*x;
    
    clear Phi
    clear Phi0
    % solving for step
    fprintf('\n\nSolving for EV-Filter Order %d...(Step Resp)\n\n',Kred);
    
    theta_star = As(:,1:N+Kred*nnz(Ls))\h_tilde;

    Phi0 = diag(theta_star(1:N));

    Phi = zeros(N,N,Kred);

    for kk = 1:Kred
        tmp = zeros(N,N);
        indx = N+1 + (kk-1)*nnz(Ls);
        tmp(maskL) = theta_star(indx : indx + nnz(Ls)-1);
        Phi(:,:,kk) = tmp;
    end
    
%     if isfinite(cvx_optval)
        yred_step = Phi0*x;

        for iK = 1 : Kred
            yred_step = yred_step + ...
                (Phi(:,:,iK) .* Ls)*Ls^(iK-1)*x;
        end
    
        
    % node variant
    Phi_nv = zeros(N,N,Kred+1);
    theta_star_nv = As_nv(:,1:(Kred+1)*N)\h_tilde;
    
    Hnv = zeros(N,N);
    for kk = 0:Kred
        indx = kk*N +1;
        Phi_nv(:,:,kk+1) = diag(theta_star_nv(indx: indx + N - 1));
        Hnv = Hnv + Phi_nv(:,:,kk+1)*Ls^kk;
    end
    
    yred_step_nv = Hnv*x;
    
    yred_f_step(:,Kred) = U'*yred_step;

    yred_f_kernel(:,Kred) = U'*yred_kernel;
    
    yred_f_step_nv(:,Kred) = U'*yred_step_nv;

    yred_f_kernel_nv(:,Kred) = U'*yred_kernel_nv;

    NMSEred_kernel(Kred) = (norm(yred_f_kernel(:,Kred)-kernelResp).^2)./(norm(kernelResp).^2);
    NMSEred_step(Kred) = (norm(yred_f_step(:,Kred)-stepResp).^2)./(norm(stepResp).^2);
    
    NMSEred_kernel_nv(Kred) = (norm(yred_f_kernel_nv(:,Kred)-kernelResp).^2)./(norm(kernelResp).^2);
    NMSEred_step_nv(Kred) = (norm(yred_f_step_nv(:,Kred)-stepResp).^2)./(norm(stepResp).^2);
    
end
%%
% close all,
figure
set(gca,'fontsize',15)
subplot(2,1,1)
p2 = plot(1:Kfull,NMSEred_kernel,'-*b','linewidth',1.1); hold on, 
p4 = plot(1:Kfull,kernelNMSEfull,'--or','linewidth',1.1);
p6 = plot(1:Kfull,NMSEred_kernel_nv,'-.+g','linewidth',1.1);
legend([p2;p4;p6],{'EV-Filter (k) Exponential Kernel',...
    'FIR (k) Exponential Kernel',...
    'NV-Filter (k) Exponential Kernel'})
xlabel('Filter Order [k]')
ylabel('NMSE')
set(gca,'fontsize',15)
ylim([eps,1.1])
yticks([10e-16 10e-10 10e-5 1])
set(gca,'yscale','log'), grid on

subplot(2,1,2)
set(gca,'fontsize',15)
p1 = plot(1:Kfull,NMSEred_step,'-*b','linewidth',1.1); hold on, 
p3 = plot(1:Kfull,stepNMSEfull,'--or','linewidth',1.1);
p5 = plot(1:Kfull,NMSEred_step_nv,'-.+g','linewidth',1.1);
legend([p1;p3;p5],{'EV-Filter (k) LP Filter',...
    'FIR (k) LP Filter', 'NV-Filter (k) LP Filter'})
xlabel('Filter Order [k]')
ylabel('NMSE')
set(gca,'fontsize',15)
ylim([0 0.6])
set(gca,'yscale','log'), grid on

%%

legend([p1;p2;p3;p4;p5;p6],{'EV-Filter (k) LP Filter','EV-Filter (k) Exponential Kernel',...
    'FIR (k) LP Filter','FIR (k) Heat Kernel', 'NV-Filter (k) LP Filter',...
    'NV-Filter (k) Exponential Kernel'})

%%
figure,
p1 = plot(diag(Lam),stepResp,'-r','linewidth',1.1); hold on,
p2 = plot(diag(Lam),yred_f_step(:,8),'+-m','linewidth',1.1);
p3 = plot(diag(Lam),stepFull_y_f(:,1),'linewidth',1.1);
p4 = plot(diag(Lam),stepFull_y_f(:,5),'linewidth',1.1);
p5 = plot(diag(Lam),stepFull_y_f(:,10),'linewidth',1.1);
p6 = plot(diag(Lam),stepFull_y_f(:,15),'linewidth',1.1);
p7 = plot(diag(Lam),stepFull_y_f(:,20),'linewidth',1.1);
p8 = plot(diag(Lam),stepFull_y_f(:,25),'linewidth',1.1);

% title('Step Function')
set(gca,'fontsize',15)
legend([p1;p2;p3;p4;p5;p6;p7;p8],{'Desired Response','Dist. Edge-Variant K = 8',...
    'FIR K =1', 'FIR K=5','FIR K=10','FIR K=15','FIR K=20','FIR K=25'},...
    'location','sw')
xlabel('Eigenvalues')
ylabel('h(l)')
set(gca,'fontsize',13)

figure
set(gca,'fontsize',13)
p1 = plot(diag(Lam),kernelResp,'-r','linewidth',1.1);
hold on, p2 = plot(diag(Lam),yred_f_kernel(:,2),'o-g','linewidth',1.1);
hold on, p22 = plot(diag(Lam),yred_f_kernel(:,3),'x-m','linewidth',1.1);
p3 = plot(diag(Lam),KernelFull_y_f(:,1),'linewidth',1.1);
p4 = plot(diag(Lam),KernelFull_y_f(:,2),'linewidth',1.1);
p5 = plot(diag(Lam),KernelFull_y_f(:,3),'linewidth',1.1);
p6 = plot(diag(Lam),KernelFull_y_f(:,4),'linewidth',1.1);
p7 = plot(diag(Lam),KernelFull_y_f(:,5),'linewidth',1.1);
legend([p1;p2;p22;p3;p4;p5;p6;p7],...
    {'Desired Response','Dist. Edge Variant K = 2',...
    'Dist. Edge Variant K = 3','FIR = 1','FIR = 2','FIR = 3',...
    'FIR = 4','FIR = 5'})
% title('Heat Kernel')
xlabel('Eigenvalues')
ylabel('h(l)')
set(gca,'fontsize',15)